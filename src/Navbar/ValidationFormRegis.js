import {useState, useEffect} from 'react';
import validateRegis from './ValidateInfoRegis.js'

const useFormRegis = vaildateRegis => {
    const [valuesRegis, setValuesRegis] = useState({
        namaRegis: '', 
        emailRegis: '',
        passwordRegis:'',

    })
    const [errorsRegis, setErrorsRegis] = useState({})
    const [isSubmitting, setIsSubmitting] = useState(false)


    const handleChangeRegis = e => {
        const {name, value} = e.target
        setValuesRegis({
            ...valuesRegis,
            [name]: value
        });
    };

    const handleSubmitRegis = e =>{
        e.preventDefault();

        setErrorsRegis(validateRegis(valuesRegis))
        setIsSubmitting(true)
    };

    return {handleChangeRegis, valuesRegis, handleSubmitRegis, errorsRegis}
}

export default useFormRegis;
