import {useState, useEffect} from 'react';
import validate from './ValidateInfoAccount.js'

const useFormLogin = vaildate => {
    const [values, setValues] = useState({
        emailLogin: '',
        passwordLogin:'',

    })
    const [errors, setErrors] = useState({})
    const [isSubmitting, setIsSubmitting] = useState(false)


    const handleChange = e => {
        const {name, value} = e.target
        setValues({
            ...values,
            [name]: value
        });
    };

    const handleSubmit = e =>{
        e.preventDefault();

        setErrors(validate(values))
        setIsSubmitting(true)
    };

    return {handleChange, values, handleSubmit, errors}
}

export default useFormLogin;
