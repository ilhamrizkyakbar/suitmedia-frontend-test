import React from 'react';
import './Navbar.css';
import './LoginModal.css';
import useFormLogin from './ValidationFormAccounts.js';
import validate from './ValidateInfoAccount.js';
import useFormRegis from './ValidationFormRegis.js';
import validateRegis from './ValidateInfoRegis.js';

const Navbar = (submitForm) => {
    const { handleChange, values, handleSubmit, errors } = useFormLogin(submitForm, validate);
    const { handleChangeRegis, valuesRegis, handleSubmitRegis, errorsRegis } = useFormRegis(submitForm, validateRegis);

    const [state, setState] = React.useState(false);
    const handleClick = () => {
        setState({ clicked: !state.clicked })
    };

    const [show, setShow] = React.useState(!true);

    const openLogin = () => {
        setShow(true)
    };

    const closeLogin = () => {
        setShow(false)
    };

    const [display, setDisplay] = React.useState(!true);

    const openRegister = () => {
        setDisplay(true)
    };

    const closeRegister = () => {
        setDisplay(false)
    };

    const ModalLogin = (props) => {
        if (show) {
            return (
                <div class="modal-container">
                    <div className="modal-backdrop" />
                    <div className="modal-box">
                        {props.children}
                        <div className="modal-header"><h2 className="modal-header">Login</h2>
                            <p className="fas fa-times modal-header" onClick={closeLogin}></p></div>
                        <div className="modal-content">
                            <form className="form-login" onSubmit={handleSubmit}>
                                <div className="form-inputs">
                                    <label className="form-label">Email</label>
                                    <input type="text" id="emailLogin" name="emailLogin" className="input-field"
                                        value={values.emailLogin} onChange={handleChange}></input>
                                    {errors.emailLogin && <p className="warning-notif">{errors.emailLogin}</p>}
                                </div>
                                <div className="form-inputs">
                                    <label className="form-label">Password</label>
                                    <input type="password" id="passwordLogin" name="passwordLogin" className="input-field"
                                        value={values.passwordLogin} onChange={handleChange}>
                                    </input>
                                    {errors.passwordLogin && <p className="warning-notif">{errors.passwordLogin}</p>}
                                </div>
                                <button className="form-login-btn" type="submit">Login</button>
                            </form>

                        </div>
                    </div>
                </div>)
        }
        return null
    }

    const ModalRegister = (props) => {
        if (display) {
            return (
                <div class="modal-container">
                    <div className="modal-backdrop" />
                    <div className="modal-box-register">
                        {props.children}
                        <div className="modal-header"><h2 className="modal-header">Register</h2>
                            <p className="fas fa-times modal-header close-btn-regis" onClick={closeRegister}></p></div>
                        <div className="modal-content">
                            <form className="form-login" onSubmit={handleSubmitRegis}>
                            <div className="form-inputs">
                                    <label className="form-label">Nama</label>
                                    <input type="text" id="namaRegis" name="namaRegis" className="input-field"
                                        value={valuesRegis.namaRegis} onChange={handleChangeRegis}></input>
                                    {errorsRegis.namaRegis && <p className="warning-notif">{errorsRegis.namaRegis}</p>}
                                </div>
                                <div className="form-inputs">
                                    <label className="form-label">Email</label>
                                    <input type="text" id="emailRegis" name="emailRegis" className="input-field" 
                                        value={valuesRegis.emailRegis} onChange={handleChangeRegis}></input>
                                    {errorsRegis.emailRegis && <p className="warning-notif">{errorsRegis.emailRegis}</p>}
                                </div>
                                <div className="form-inputs">
                                    <label className="form-label">Password</label>
                                    <input type="password" id="passwordRegis" name="passwordRegis" className="input-field"
                                        value={valuesRegis.passwordRegis} onChange={handleChangeRegis}>
                                    </input>
                                    {errorsRegis.passwordRegis && <p className="warning-notif">{errorsRegis.passwordRegis}</p>}
                                </div>
                                <button className="form-register-btn" type="submit">Register</button>
                            </form>

                        </div>
                    </div>
                </div>)
        }
        return null
    }

    return (
        <nav className="Navbar">
            <h1 className="LogoNavbar">Forum anak IT</h1>
            <div className="icon-container">
                <div className="menu-icon" onClick={handleClick}>
                    <i className={state.clicked ? 'fas fa-times' : 'fas fa-bars'}></i>
                </div>
            </div>
            <div className="NavbarMenu">
                <ul className={state.clicked ? 'nav-menu active' : 'nav-menu'}>
                    <li className="SearchBar">
                        <input type="search" placeholder="Search"></input>
                        <span className="fas fa-search"></span>
                    </li>
                    <li className="menu-holder">
                        <a className="nav-links" id="categories" href="#">
                            Categories
                        <div className="Dropdown">
                                <li className="dropdown-link">
                                    <a href="#">
                                        Linux
                            </a>
                                </li>
                                <li className="dropdown-link">
                                    <a href="#">
                                        Windows
                            </a>
                                </li>
                                <li className="dropdown-link">
                                    <a href="#">
                                        Mac OS
                            </a>
                                </li>
                                <li className="dropdown-link">
                                    <a href="#">
                                        Android
                            </a>
                                </li>
                                <li className="dropdown-link">
                                    <a href="#">
                                        iOS
                            </a>
                                </li>
                            </div>
                        </a>
                        <a className="nav-links" id="Login" href="#" onClick={openLogin}>
                            Login
                    </a>
                        <a className="nav-links" id="Register" href="#" onClick={openRegister}>
                            Register
                    </a>
                    </li>
                </ul>
            </div>
            <ModalLogin></ModalLogin>
            <ModalRegister></ModalRegister>
        </nav>
    );
}

export default Navbar