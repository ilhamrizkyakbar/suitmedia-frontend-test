export default function ValidateInfoRegis(values){
    let errorsRegis = {}

    if(!values.namaRegis){
        errorsRegis.namaRegis = "Wajib diisi"
    }

    if(!values.passwordRegis){
        errorsRegis.passwordRegis = "Wajib diisi"
    }

    if (!values.emailRegis){
        errorsRegis.emailRegis = "Wajib diisi"
    }
    else if(!values.emailRegis.includes("@")){
        errorsRegis.emailRegis = "Format email salah"
    }

    return errorsRegis;
}