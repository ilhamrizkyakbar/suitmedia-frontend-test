export default function ValidateInfo(values){
    let errors = {}

    if(!values.passwordLogin){
        errors.passwordLogin = "Wajib diisi"
    }

    if (!values.emailLogin){
        errors.emailLogin = "Wajib diisi"
    }
    else if(!values.emailLogin.includes("@")){
        errors.emailLogin = "Format email salah"
    }
    else if(values.emailLogin.includes("@")){
    }

    return errors;
}