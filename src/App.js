import React from 'react';
import './App.css';
import Navbar from './Navbar/Navbar.js'
import Content from './Content/Content.js'

function App() {
  return (
    <div className="App">
      <Navbar></Navbar>
      <Content></Content>
    </div>
  );
}

export default App;
