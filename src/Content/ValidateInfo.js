export default function ValidateInfo(values){
    let errors = {}

    if(!values.nama){
        errors.nama = "Wajib diisi"
        document.getElementById("nama").style.borderColor = "#FF5F2D";
    }
    else if(values.nama){
        document.getElementById("nama").style.borderColor = "#33B95A";
    }

    if (!values.email){
        errors.email = "Wajib diisi"
        document.getElementById("email").style.borderColor = "#FF5F2D";
    }
    else if(!values.email.includes("@")){
        errors.email = "Format email salah"
        document.getElementById("email").style.borderColor = "#FF5F2D";
    }
    else if(values.email.includes("@")){
        document.getElementById("email").style.borderColor = "#33B95A";
    }

    if(!values.komentar){
        errors.komentar = "Wajib diisi"
        document.getElementById("komentar").style.borderColor = "#FF5F2D";
    }
    else if(values.komentar){
        document.getElementById("komentar").style.borderColor = "#33B95A";
    }

    return errors;
}