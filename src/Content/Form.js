import React, { useState } from 'react';
import FormKomentar from './TambahKomentar.js';

const Form = () => {
    const [isSubmitted, setIsSubmitted] = useState(false)

    function submitForm() {
        setIsSubmitted(true)
    }
    return(
        <div>
            {!isSubmitted ? <FormKomentar submitForm={submitForm}></FormKomentar> : window.location.reload()}
        </div>
    )
}

export default Form; 