import {useState, useEffect} from 'react';
import validate from './ValidateInfo.js'

const useForm = vaildate => {
    const [values, setValues] = useState({
        nama: '',
        email: '',
        komentar:''
    })
    const [errors, setErrors] = useState({})
    const [isSubmitting, setIsSubmitting] = useState(false)


    const handleChange = e => {
        const {name, value} = e.target
        setValues({
            ...values,
            [name]: value
        });
    };


    const handleSubmit = e =>{
        e.preventDefault();

        setErrors(validate(values))
        setIsSubmitting(true)
    };

    const handleReset = () => {
        Array.from(document.querySelectorAll("input")).forEach(
          input => (input.value = "")
        );
        document.getElementById("komentar").innerHTML = "";
        document.getElementById("komentar").style.borderColor = "#71695F";
        document.getElementById("email").style.borderColor = "#71695F";
        document.getElementById("nama").style.borderColor = "#71695F";
        setValues({
          itemvalues: [{}]
        });
        setErrors({
          errors: [{}],
        })
    };


    return {handleChange, values, handleSubmit, errors, handleReset}
}

export default useForm;
