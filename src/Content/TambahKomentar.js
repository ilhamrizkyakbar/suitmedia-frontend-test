import React, { useState } from 'react';
import './TambahKomentar.css';
import useForm from './ValidationFrom.js';
import validate from './ValidateInfo.js';

const FormKomentar = (submitForm) => {
    const {handleChange, values, handleSubmit, errors, handleReset} = useForm(submitForm, validate);

    return (
        <form className="form-comment" onSubmit={handleSubmit}>
            <div className="form-inputs">
                <input type="text" id="nama" name="nama" className="input-field" placeholder="Nama"
                value={values.nama} onChange={handleChange}></input>
                {errors.nama && <p className="warning-notif">{errors.nama}</p>}
            </div>
            <div className="form-inputs">
                <input type="text" id="email" name="email" className="input-field" placeholder="Email"
                value={values.email} onChange={handleChange}></input>
                {errors.email && <p className="warning-notif">{errors.email}</p>}
            </div>
            <div className="form-inputs">
                <textarea id="komentar" name="komentar" className="input-field-textarea" placeholder="Komentar anda"
                value={values.komentar} onChange={handleChange}></textarea>
                {errors.komentar && <p className="warning-notif">{errors.komentar}</p>}
            </div>
            <button className="form-reset-btn" type="reset" onClick={handleReset}>Reset</button>
            <button className="form-comment-btn" type="submit">Submit</button>
        </form>
    )
}


export default FormKomentar;