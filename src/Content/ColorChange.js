import React from 'react';
import './ColorChange.css';

class ColorChange extends React.Component {

    constructor(){
        super();

        this.state = {
            notGreen: true,
            isButtonDisabled: false,
            notRed: true
        }
   }
    
    changeColor(event) {
        this.setState({ notGreen: !this.state.notGreen })
        this.setState({
            isButtonDisabled: true
          });
        const parentID = String(event.target.parentElement.parentElement.id);
        const idPoint = "point-"
        const pointCatch = parentID.substr(-1, );
        const pointToAdd = idPoint + pointCatch;
        const pointReceiver = parseInt(document.getElementById(pointToAdd).innerHTML);
        const addPoint = pointReceiver + 1;
        document.getElementById(pointToAdd).innerHTML = addPoint;
    }

    changeColorRed(event) {
        this.setState({ notRed: !this.state.notRed })
        this.setState({
            isButtonDisabled: true
          });
          const parentID = String(event.target.parentElement.parentElement.id);
          const idPoint = "point-"
          const pointCatch = parentID.substr(-1, );
          const pointToSubs = idPoint + pointCatch;
          const pointReceiver = parseInt(document.getElementById(pointToSubs).innerHTML);
          const subsPoint = pointReceiver - 1;
          document.getElementById(pointToSubs).innerHTML = subsPoint;
        
    }

    render() {
        let btn_class = this.state.notGreen ? "fas fa-arrow-up btn-vote not-green-btn" : "fas fa-arrow-up btn-vote green-btn";
        let btn_class_red = this.state.notRed ? "fas fa-arrow-down btn-vote not-red-btn" : "fas fa-arrow-down btn-vote red-btn";

        return (
            <div className="btn-group 0">
                <button disabled={this.state.isButtonDisabled} id="btn-green" className={btn_class} onClick={this.changeColor.bind(this)}></button>
                <button disabled={this.state.isButtonDisabled} className={btn_class_red} onClick={this.changeColorRed.bind(this)}></button>
            </div>
        )
    }
}

export default ColorChange;