import React from 'react';
import './Content.css';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import Komentar from './Komentar.js';
import Form from './Form.js';

const useStyles = makeStyles((theme) => ({
    grid: {
        width: '100%',
        margin: '0px',
    }
}));

const styleDiskusi = {
    paddingLeft: '15%',
};

const discussionList = [
    { judulDiskusi: 'Bersihkan laptop dari butiran debu', key: 1 },
    { judulDiskusi: 'Cara akses website menggunakan koneksi OpenVPN', key: 2 },
    { judulDiskusi: 'Batas aman overclock PC rakitan', key: 3 },
    { judulDiskusi: 'Cara mengetahui akun Facebook di-hack melalui aplikasi', key: 4 },
    { judulDiskusi: 'Tutorial: langkah-langkah mencegah website untuk track user', key: 5 },
];

function Content() {
    const classes = useStyles();
    return (
        <Grid container spacing={2} className={classes.grid}>
            <Grid item xs={12} md={6}>
                <div className="MainIssue">
                    <h2 className="title">Lampu webcam tiba-tiba menyala sendiri tanpa membuka aplikasi webcam</h2>
                    <p className="pertanyaan">
                        Mau tanya, akhir-akhir ini webcam sering nyala sendiri. Apakah ada yang tahu penyebabnya dan
                        solusi untuk memperbaiki hal itu? Apakah ada kemungkinan laptop saya di-hack karena kasus
                        terjadi tiap terkoneksi di internet.
                    </p>
                </div>
                <div className="Komentar">
                    <h3 className="title-komentar">Komentar</h3>
                    <Komentar></Komentar>
                </div>
                <div className="komentar-tambah">
                    <h3 className="title-tambah-komentar">Tambahkan Komentar</h3>
                    <Form></Form>
                </div>
            </Grid>
            <Grid item xs={12} md={6}>
                <div className="Discussion" style={styleDiskusi}>
                    <h3 className="title-right">Diskusi 5 Teratas</h3>
                    <p className="diskusi-title">
                        {discussionList.map(judul => {
                            return (
                                <Grid container spacing={2} className={classes.grid}>
                                    <Grid item xs={1}>
                                        <p className="title-numbering">{judul.key}.</p>
                                    </Grid>
                                    <Grid item xs={8}>
                                        <li key={judul.key} className="judul-diskusi">{judul.judulDiskusi}</li>
                                    </Grid>
                                </Grid>
                            );
                        })}
                    </p>
                </div>
            </Grid>
        </Grid>
    )
}

export default Content