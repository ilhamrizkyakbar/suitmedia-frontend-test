import React from 'react';
import './Komentar.css';
import Comments from './comments.json';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import { format } from "date-fns";
import { render } from 'react-dom';
import ColorChange from './ColorChange.js'

const useStyles = (theme => ({
    grid: {
        width: '100%',
        margin: '0px',
    }
}));

function formatDate(string) {
    var date = new Date(string)
    var formattedDate = format(date, "MMMM dd, yyyy H:mm")
    return formattedDate;
}

class Komentar extends React.Component {
    render(){
        const {classes} = this.props;
        return (
            <div>
                <Grid container spacing={3} className={classes.grid}>
                    <Grid item xs={10}>
                        <Grid container spacing={2} className={classes.grid}>
                            <Grid item xs={3}>
                                <img src={Comments[0].avatar} className="main-comment-pic" />
                            </Grid>
                            <Grid item xs={9}>
                                <h4 className="author-name">{Comments[0].author}</h4>
                                <p className="date-comment">{formatDate(Comments[0].date)}</p>
                                <p className="comment-message">{Comments[0].message}</p>
                                <p className="button-area" id="button-area-0"><p className="button-area" key={Comments[0].id} id="point-0">{Comments[0].point}</p> Point
                                <ColorChange></ColorChange>
                                </p>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid item xs={10}>
                            <Grid container spacing={2} className={classes.grid}>
                                <Grid item xs={3}>
                                    <img src={Comments[0].replies[0].avatar} className="reply-comment-pic" />
                                </Grid>
                                <Grid item xs={9}>
                                    <h4 className="author-name-reply">{Comments[0].replies[0].author}</h4>
                                    <p className="date-comment-reply">{formatDate(Comments[0].replies[0].date)}</p>
                                    <p className="comment-message-reply">{Comments[0].replies[0].message}</p>
                                    <p className="button-area-reply" id="button-area-1"><p className="button-area-reply" key={Comments[0].replies[0].id} id="point-1">{Comments[0].replies[0].point}</p> Point
                                <ColorChange></ColorChange>
                                </p>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container spacing={3} className={classes.grid}>
                    <Grid item xs={10}>
                        <Grid container spacing={2} className={classes.grid}>
                            <Grid item xs={3}>
                                <img src={Comments[1].avatar} className="main-comment-pic" />
                            </Grid>
                            <Grid item xs={9}>
                                <h4 className="author-name">{Comments[1].author}</h4>
                                <p className="date-comment">{formatDate(Comments[1].date)}</p>
                                <p className="comment-message">{Comments[1].message}</p>
                                <p className="button-area" id="button-area-2"><p className="button-area" id="point-2" key={Comments[1].id}>{Comments[1].point}</p> Point
                                <ColorChange></ColorChange>
                                </p>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Grid item xs={10}>
                            <Grid container spacing={2} className={classes.grid}>
                                <Grid item xs={3}>
                                    <img src={Comments[1].replies[0].avatar} className="reply-comment-pic" />
                                </Grid>
                                <Grid item xs={9}>
                                    <h4 className="author-name-reply">{Comments[1].replies[0].author}</h4>
                                    <p className="date-comment-reply">{formatDate(Comments[1].replies[0].date)}</p>
                                    <p className="comment-message-reply">{Comments[1].replies[0].message}</p>
                                    <p className="button-area-reply" id="button-area-3"><p className="button-area-reply" id="point-3" key={Comments[1].replies[0].id}>{Comments[1].replies[0].point}</p> Point
                                <ColorChange></ColorChange>
                                </p>
                                </Grid>
                            </Grid>
                            <Grid container spacing={2} className={classes.grid}>
                                <Grid item xs={3}>
                                    <img src={Comments[1].replies[1].avatar} className="reply-comment-pic" />
                                </Grid>
                                <Grid item xs={9}>
                                    <h4 className="author-name-reply">{Comments[1].replies[1].author}</h4>
                                    <p className="date-comment-reply">{formatDate(Comments[1].replies[1].date)}</p>
                                    <p className="comment-message-reply">{Comments[1].replies[1].message}</p>
                                    <p className="button-area-reply" id="button-area-4"><p className="button-area-reply" id="point-4" key={Comments[1].replies[1].id}>{Comments[1].replies[1].point}</p> Point
                                <ColorChange></ColorChange>
                                </p>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container spacing={3} className={classes.grid}>
                    <Grid item xs={10}>
                        <Grid container spacing={2} className={classes.grid}>
                            <Grid item xs={3}>
                                <img src={Comments[2].avatar} className="main-comment-pic" />
                            </Grid>
                            <Grid item xs={9}>
                                <h4 className="author-name">{Comments[2].author}</h4>
                                <p className="date-comment">{formatDate(Comments[2].date)}</p>
                                <p className="comment-message">{Comments[2].message}</p>
                                <p className="button-area" id="button-area-5"><p className="button-area" id="point-5" key={Comments[2].id}>{Comments[2].point}</p> Point
                                <ColorChange></ColorChange>
                                </p>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        )

    }
}

export default withStyles(useStyles, { withTheme: true })(Komentar);